<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUs;
use App\Http\Resources\AdvCollection;
use App\Http\Resources\CityCollection;
use App\Http\Resources\HomeResource;
use App\Http\Resources\PhoneColorCollection;
use App\Http\Resources\PhoneModelCollection;
use App\Http\Resources\PhoneTypeCollection;
use App\Http\Resources\ProblemCollection;
use App\Http\Resources\Setting as SettingResource;
use App\Http\Resources\StateCollection;
use App\Models\Adv;
use App\Models\City;
use App\Models\Contact;
use App\Models\PhoneColor;
use App\Models\PhoneModel;
use App\Models\PhoneTypes;
use App\Models\Problem;
use App\Models\Setting;
use App\Models\State;
use Helpers;

class HomeController extends Controller
{
    /**
     * @param $lng
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLanguage($lng)
    {

        $message = session(['locale' => $lng]);
        return response()->json($message, 204);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities()
    {
        $cities = City::all();
        return response()->json(new CityCollection($cities), 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdvs()
    {
        $advs = Adv::all();
        return response()->json(new AdvCollection($advs), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhoneTypes()
    {
        $phonetypes = PhoneTypes::all();
        return response()->json(new PhoneTypeCollection($phonetypes), 200);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhoneModels($id)
    {
        $phone_type = PhoneTypes::find($id);
        if (!$phone_type) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $phonemodels = PhoneModel::where('phone_type_id', $id)->get();
        return response()->json(new PhoneModelCollection($phonemodels), 200);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStates($id)
    {
        $city = City::find($id);
        if (!$city) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $states = State::where('city_id', $id)->get();
        return response()->json(new StateCollection($states), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSettings()
    {
        $settings = Setting::first();
        return response()->json(new SettingResource($settings), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHome()
    {
        $settings = Setting::first();
        return response()->json(new HomeResource($settings), 200);

    }

    /**
     * @param ContactUs $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactUs(ContactUs $request)
    {
        $data = $request->all();
        Contact::create($data);
        return response()->json([], 204);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhoneColors($id)
    {
        $phonemodel = PhoneModel::find($id);
        if (!$phonemodel) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $colors = PhoneColor::where('phone_model_id', $id)->get();
        return response()->json(new PhoneColorCollection($colors), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProblems($phone_model_id)
    {
        $problems = Problem::where('phone_model_id', $phone_model_id)->get();
        if (!$problems) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        return response()->json(new ProblemCollection($problems), 200);
    }


}


