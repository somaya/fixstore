<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccessoriesCollection;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection as ProductCollectionResource;
use App\Models\Accessory;
use App\Models\Favourite;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * @param $category
     * @param null $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts($category, $type = null)
    {
        $products = Product::where('category', $category)->where('active', 1)->orderBy('created_at', 'desc');
        //filter new phones or used phones with phone types
        if ($type && ($category == 1 || $category == 2)) {
            $products = $products->where('phone_type_id', $type)->paginate(4);
        }
        //filter accessories with acessory types

        if ($type && $category == 3) {
            $products = $products->where('accessory_id', $type)->paginate(4);
        }
        //no filter
        if (!$type) {
            $products = $products->paginate(4);
        }
        return response()->json(new ProductCollectionResource($products), 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavouriteProducts()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $productIds = Favourite::where('user_id', $user->id)->pluck('product_id');
        $products = Product::whereIn('id', $productIds)->get();

        return response()->json(new ProductCollectionResource($products), 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeFavouriteProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
        ]);

        $inputs = $request->all();
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $product = Product::find($request->product_id);
        if (!$product || $product == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $inputs['user_id'] = $user->id;

        $fav = Favourite::where(['user_id' => $user->id, 'product_id' => $product->id])->first();

        if ($fav)
            $fav->delete();
        else
            Favourite::firstOrCreate($inputs);

        return response()->json([], 204);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProduct(Request $request)
    {

        $rules = [
            'category' => 'required',
            'title' => 'required',
            'photo' => 'required',
            'price' => 'required',
            'description' => 'required',
            'address' => 'required',
        ];

        if ($request->category == 1 || $request->category == 2) {
            $rules['phone_type_id'] = 'required';
            $rules['phone_model_id'] = 'required';
            $rules['phone_color_id'] = 'required';
            $rules['rams'] = 'required';
            $rules['memory'] = 'required';
            $rules['processor'] = 'required';
            $rules['screen'] = 'required';
        }
        if ($request->category == 3) {
            $rules['accessory_id'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $data = $request->except('photo');
        $product = Product::create(array_merge($data, ['user_id' => $user->id]));
        if ($request->photo) {
            $imageName = str_random(10) . '.' . $request->photo->extension();
            $request->photo->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $product->photo = 'uploads/products/' . $imageName;
            $product->save();
        }

        return response()->json([], 204);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editProduct(Request $request)
    {
        $rules = [
            'product_id' => 'required',
            'category' => 'required',
            'title' => 'required',
            'price' => 'required',
            'description' => 'required',
            'address' => 'required',
        ];

        if ($request->category == 1 || $request->category == 2) {
            $rules['phone_type_id'] = 'required';
            $rules['phone_model_id'] = 'required';
            $rules['phone_color_id'] = 'required';
            $rules['rams'] = 'required';
            $rules['memory'] = 'required';
            $rules['processor'] = 'required';
            $rules['screen'] = 'required';
        }
        if ($request->category == 3) {
            $rules['accessory_id'] = 'required';

        }
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $product = Product::find($request->product_id);
        if (!$product || $product == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $data = $request->except(['photo', 'product_id']);
        $product->update($data);
        if ($request->hasfile('photo') || $request->photo != null) {
            $imageName = str_random(10) . '.' . $request->photo->extension();
            $request->photo->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $product->photo = 'uploads/products/' . $imageName;
            $product->save();
        }

        return response()->json([], 204);

    }

    /**
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductDetails($product_id)
    {
        $product = Product::find($product_id);
        if (!$product || $product == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        return response()->json(new ProductResource($product), 200);
    }


    /**
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function stopProduct($product_id)
    {
        $product = Product::find($product_id);
        if (!$product || $product == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $product->update([
            'active' => 0
        ]);
        return response()->json([], 204);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyProducts()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $products = Product::where('user_id', $user->id)->where('active', 1)->get();

        return response()->json(new ProductCollectionResource($products), 200);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAccessories()
    {
        $accessory = Accessory::all();
        return response()->json(new AccessoriesCollection($accessory), 200);
    }
}
