<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\CompleteProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\AddressCollection;
use App\Http\Resources\UserProfileResource;
use App\Models\Notification;
use App\Models\FixOrder;
use App\Models\UserAddress;
use App\User;
use Carbon\Carbon;
use Helpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = User::firstOrCreate([
            'phone' => request('phone')
        ]);

        if ($user) {
            $code = rand(1111, 9999);
//            $code = 5698;
            $user->update([
                'login_code' => $code,
//                'code_expire_at' => Carbon::now()->addMinutes(5)
                'code_expire_at' => Carbon::now()->addMonths(5)
            ]);

            $messageContent = 'كود التفعيل : ' . $code;
            Helpers::send_sms($messageContent, $user->phone);
        }
        return response()->json([
            'user_id' => $user->id,
            'login_code' => $user->login_code,
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login_code' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $login_code = $request->login_code;
        $user_id = $request->user_id;

        $user = User::find($user_id);

        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        if ($user->login_code == $login_code) {
            if (Carbon::now() > $user->code_expire_at) {
                if (request()->header('Accept-Language') == 'ar')
                    $message = 'الكود منتهي .. برجاء الحصول على كود جديد';
                else
                    $message = 'expired login code, get new one';
                return response()->json(['error' => $message], 400);
            }

            if ($user->tokens == null) {
                $access_token = Helpers::generateRandomString();
                $user->update([
                    'tokens' => $access_token,
                ]);
            }
            $user->type == 1 ? $user_type = 'user' : $user_type = 'engineer';

            Helpers::updateFCMToken($user);

            return response()->json([
                'complete_data' => $user->name != null ? true : false,
                'user_type' => $user_type,
                'user_name' => $user->name ? $user->name : '',
                'user_image' => $user->photo ? \Helpers::base_url() . '/' . $user->photo : '',
                'joined_from' => $user->created_at ? $user->created_at->toDateString() : '',
                'fcm_token' => $user->device_token,
                'access_token' => $user->tokens,
            ], 200);

        } else {
            if (request()->header('Accept-Language') == 'ar')
                $message = 'الكود غير صالح';
            else
                $message = 'Invalid login code';
            return response()->json(['error' => $message], 400);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resendUserActivationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find($request->user_id);

        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

        $code = rand(1111, 9999);
//        $code = 5698;
        $user->update([
            'login_code' => $code,
        ]);

        $messageContent = 'كود التفعيل : ' . $code;
        Helpers::send_sms($messageContent, $user->phone);

        $user->save();

        return response()->json([], 204);
    }


    /**
     * @param CompleteProfileRequest $request
     * @return JsonResponse
     */
    public function completeProfile(CompleteProfileRequest $request)
    {
        $user = User::find($request->user_id);

        $user->update([
            'name' => $request->name,
            'city_id' => $request->city_id,
            'state_id' => $request->state_id,
        ]);

        Helpers::updateFCMToken($user);
        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function getProfile()
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

        return response()->json(new UserProfileResource($user), 200);
    }

    /**
     * @param Request $request
     * @return mixed|void
     */
    public function changeProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png,gif|required|image',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();

        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $filepath = 'images/users/' . date('Y') . '/' . date('m') . '/';
            $filename = $filepath . time() . '-' . $file->getClientOriginalName();
            $file->move($filepath, $filename);
            $user->photo = $filename;
            $user->save();
        }

        return response()->json(['image' => \Helpers::base_url() . '/' . $user->photo], 200);
    }


    /**
     * @return JsonResponse
     */
    public function logout()
    {
        $user = Helpers::getLoggedUser();
        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $user->update([
            'device_token' => null
        ]);
        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function getUserAddresses()
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $addresses = UserAddress::where('user_id', $user->id)->get();

        return response()->json(new AddressCollection($addresses), 200);
    }

    /**
     * @param UpdateProfileRequest $request
     * @return JsonResponse
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $user->update([
            'name' => $request->name,
            'city_id' => $request->city_id,
            'state_id' => $request->state_id,
            'phone' => $request->phone
        ]);
        return response()->json([], 204);
    }


    /**
     * @param Request $request
     * @return mixed|string
     *
     */
    public function updateOldPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        if (Hash::check($request->old_password, $user->password)) {

            if (Hash::check($request->new_password, $user->password)) {
                return response()->json(['error' => 'same_old_password'], 400);
            } else {
                $user->update([
                    'password' => bcrypt($request->new_password)
                ]);
                return response()->json([], 204);
            }
        } else {
            return response()->json(['error' => 'wrong password'], 400);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city_id' => 'required',
            'state_id' => 'required',
            'full_address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ],
            [
                'city_id.required' => trans('messages.add_address_city_id_required'),
                'state_id.required' => 'state_id required',
                'full_address.required' => trans('messages.add_address_full_address_required'),
                'latitude.required' => trans('messages.add_address_latitude_required'),
                'longitude.required' => trans('messages.add_address_longitude_required'),
            ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $addrsss = UserAddress::create([
            'user_id' => $user->id,
            'city_id' => (int)$request->city_id,
            'state_id' => (int)$request->state_id,
            'full_address' => $request->full_address,
            'latitude' => (string)$request->latitude,
            'longitude' => (string)$request->longitude,
        ]);
        return response()->json($addrsss, 200);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deleteAddress($id)
    {
        $address = UserAddress::find($id);
        if (!$address) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $address->delete();
        return response()->json([], 204);

    }

    /**
     * @return JsonResponse
     */
    public function getEngineerStatics()
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $statics = [];
        $statics['completed_orders'] = FixOrder::where('engineer_id', $user->id)
            ->where('status', 'Completed')->count();
        $statics['accepted_orders'] = FixOrder::where('engineer_id', $user->id)
            ->where('status', 'Active')->count();
        $total = FixOrder::where('engineer_id', $user->id)
            ->where('status', 'Completed')
            ->value(\DB::raw("SUM(service_price + price)"));
        $statics['total_profits'] = (double)$total;

        return response()->json($statics, 200);
    }


    /**
     * @return JsonResponse
     */
    public function getNotifications()
    {
        $user = Helpers::getLoggedUser();
        $notifications = Notification::where('notifiable_id', $user->id)->latest()
            ->select('id', 'data', 'read_at', 'created_at')
            ->get();
        foreach ($notifications as $notification) {
            $notification->data = json_decode($notification->data);
        }
        $user->unreadNotifications->markAsRead();
        return response()->json($notifications, 200);
    }

    /**
     * @return JsonResponse
     */
    public function deleteNotification()
    {
        $user = Helpers::getLoggedUser();
        Notification::where('notifiable_id', $user->id)->delete();
        return response()->json([], 204);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deleteOneNotification($id)
    {
        $note = Notification::where('id', $id)->first();
        if ($note) {
            $note->delete();
            return response()->json([], 204);
        } else {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
    }

    /**
     * @return JsonResponse
     */
    public function unreadNotifications()
    {
        $user = Helpers::getLoggedUser();
        $count = $user->unreadNotifications->count();
        return response()->json(['count' => $count], 200);
    }
}
