<?php

namespace App\Http\Controllers\admin;

use App\Models\Accessory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccessoriesTypesController extends Controller
{
    public function index()
    {
        $accessoryTypes = Accessory::paginate(10);
        return view('admin.accessoryTypes.index', compact('accessoryTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accessoryTypes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        Accessory::create($request->all());
        return redirect('/webadmin/accessory_types')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة نوع الاكسسوار بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accessoryType = Accessory::find($id);
        return view('admin.accessoryTypes.edit', compact('accessoryType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        Accessory::find($id)->update($request->all());
        return redirect('/webadmin/accessory_types')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل نوع الاكسسوار بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Accessory::destroy($id);
        return redirect('/webadmin/accessory_types')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف نوع الاكسسوار بنجاح']));

    }
}
