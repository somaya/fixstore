<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Models\FixOrder;
use App\Models\Product;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::where('role', 2)->where('type', 1)->count();
        $engineers = User::where('role', 2)->where('type', 2)->count();
        $pending_orders = FixOrder::where('status', 'Pending')->count();
        $active_orders = FixOrder::where('status', 'Active')->count();
        $completed_orders = FixOrder::where('status', 'Completed')->count();
        $canceled_orders = FixOrder::where('status', 'Canceled')->count();
        $products = Product::count();
        return view('admin.index', compact('users', 'active_orders', 'canceled_orders', 'engineers', 'pending_orders', 'completed_orders', 'products'));
    }
}
