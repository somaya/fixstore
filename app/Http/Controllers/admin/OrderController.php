<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\FixOrder;
use App\Models\Setting;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = FixOrder::get();
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $order = FixOrder::find($id);
        return view('admin.orders.show', compact('order'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $order = FixOrder::find($id);
        $engineers = User::where('role', 2)->where('type', 2)
            ->where('ban', 0)->get();
        return view('admin.orders.edit', compact('order', 'engineers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = FixOrder::find($id);

        $order->update([
            'status' => $request->status,
            'engineer_id' => $request->engineer_id
        ]);

        if ($request->status == 'Completed') {
            $order->update([
                'tracking' => 5,
            ]);
            $setting = Setting::first();
            Transaction::updateOrCreate([
                'user_id' => $order->engineer_id,
                'order_id' => $order->id,
                'amount' => $setting->fix_app_fees
            ]);
        } elseif ($request->status == 'Pending' || $request->status == 'Canceled') {
            $order->update([
                'tracking' => 1,
            ]);
        } elseif ($request->status == 'Active') {
            $order->update([
                'tracking' => 2,
            ]);
        }

        if ($order->engineer_id = !$request->engineer_id) {
            $total_price = $order->price + $order->service_price;
            // send sms to engineers
            $messageContent = 'عزيزي مزود الخدمة ' . "\n";
            $messageContent .= 'لديك طلب رقم : ' . $order->id . "\n";
            $messageContent .= 'نوع الجهاز : ' . $order->type->name_ar . "\n";
            $messageContent .= 'موديل الجهاز : ' . $order->model->name_ar . "\n";
            $messageContent .= 'المشكلة : ' . $order->problem->name_ar . "\n";
            $messageContent .= 'الخدمة : صيانة في الموقع' . "\n";
            $messageContent .= 'السعر : ' . (double)number_format($total_price, 2, '.', '') . "\n";
            $messageContent .= 'الجوال : ' . $order->user->phone . "\n";
            $messageContent .= 'الموقع : ' . $order->address->city->name_ar . ' - ' . $order->address->full_address . "\n";
            \Helpers::send_sms($messageContent, $order->engineer->phone);
        }
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الطلب بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = FixOrder::find($id);
        $order->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف الطلب بنجاح']));

    }
}
