<?php

namespace App\Http\Controllers\admin;

use App\Models\PhoneColor;
use App\Models\PhoneModel;
use App\Models\PhoneTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhoneColorsController extends Controller
{
    public function index()
    {
        $phoneTypes = PhoneTypes::find(1);
        $phoneColors = PhoneColor::whereHas('phoneModel', function ($q) use ($phoneTypes) {
            $q->where('phone_type_id', $phoneTypes->id);
        })->paginate(10);
        return view('admin.phoneColors.index', compact('phoneColors'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listHuawei()
    {
        $phoneTypes = PhoneTypes::where('name_en', 'huawei')->first();
        $phoneColors = PhoneColor::whereHas('phoneModel', function ($q) use ($phoneTypes) {
            $q->where('phone_type_id', $phoneTypes->id);
        })->paginate(10);
        return view('admin.phoneColors.index', compact('phoneColors'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listSamsung()
    {
        $phoneTypes = PhoneTypes::where('name_en', 'samsung')->first();
        if ($phoneTypes) {
            $phoneColors = PhoneColor::whereHas('phoneModel', function ($q) use ($phoneTypes) {
                $q->where('phone_type_id', $phoneTypes->id);
            })->paginate(10);
        }
        return view('admin.phoneColors.index', compact('phoneColors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $phoneModels = PhoneModel::all();
        return view('admin.phoneColors.add', compact('phoneModels'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'phone_model_id' => 'required',
        ]);
        PhoneColor::create($request->all());
        $phoneModel = PhoneModel::find($request->phone_model_id);
        if ($phoneModel->phone_type_id == 1)
            $redirect = 'phone_colors';
        elseif ($phoneModel->phone_type_id == 2)
            $redirect = 'phone_colors/huawei';
        else
            $redirect = 'phone_colors/samsung';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تمت الاضافة بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phoneModels = phoneModel::all();
        $phoneColor = PhoneColor::find($id);
        return view('admin.phoneColors.edit', compact('phoneModels', 'phoneColor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'phone_model_id' => 'required',
        ]);
        PhoneColor::find($id)->update($request->all());
        $phoneModel = PhoneModel::find($request->phone_model_id);
        if ($phoneModel->phone_type_id == 1)
            $redirect = 'phone_colors';
        elseif ($phoneModel->phone_type_id == 2)
            $redirect = 'phone_colors/huawei';
        else
            $redirect = 'phone_colors/samsung';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل لون الهاتف بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhoneColor::destroy($id);
        return redirect('/webadmin/phone_colors')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف نوع الهاتف بنجاح']));
    }
}
