<?php

namespace App\Http\Controllers\admin;

use App\Models\PhoneModel;
use App\Models\PhoneTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhoneModelsController extends Controller
{
    public function index()
    {
        $phoneModels = PhoneModel::where('phone_type_id', 1)->paginate(10);
        return view('admin.phoneModels.index', compact('phoneModels'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listHuawei()
    {
        $phoneModels = PhoneModel::where('phone_type_id', 2)->paginate(10);
        return view('admin.phoneModels.index', compact('phoneModels'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listSamsung()
    {
        $phoneModels = PhoneModel::where('phone_type_id', 3)->paginate(10);
        return view('admin.phoneModels.index', compact('phoneModels'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $phoneTypes = PhoneTypes::all();
        return view('admin.phoneModels.add', compact('phoneTypes'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
            'phone_type_id' => 'required',
        ]);
        PhoneModel::create($request->all());


        if ($request->phone_type_id == 1)
            $redirect = 'phone_models';
        elseif ($request->phone_type_id == 2)
            $redirect = 'phone_models/huawei';
        else
            $redirect = 'phone_models/samsung';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تمت الاضافة بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phoneTypes = PhoneTypes::all();
        $phoneModel = PhoneModel::find($id);
        return view('admin.phoneModels.edit', compact('phoneTypes', 'phoneModel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'phone_type_id' => 'required',
        ]);
        PhoneModel::find($id)->update($request->all());
        if ($request->phone_type_id == 1)
            $redirect = 'phone_models';
        elseif ($request->phone_type_id == 2)
            $redirect = 'phone_models/huawei';
        else
            $redirect = 'phone_models/samsung';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم التعديل بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhoneModel::destroy($id);
        return redirect('/webadmin/phone_models')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف موديل الهاتف بنجاح']));
    }
}
