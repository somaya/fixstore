<?php

namespace App\Http\Controllers\admin;

use App\Models\PhoneTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhoneTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phoneTypes=PhoneTypes::orderBy('created_at','ASC')->paginate(10);
        return view('admin.phoneTypes.index',compact('phoneTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.phoneTypes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
       $phoneType= PhoneTypes::create($request->except('icon'));

        if ($request->hasFile('icon')) {
            $request->validate([

                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/phoneTypes/', $imageName
            );
            $phoneType->icon = '/uploads/phoneTypes/' . $imageName;
            $phoneType->save();

        }
        return redirect('/webadmin/phone_types')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة نوع الهاتف بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phoneType=PhoneTypes::find($id);
        return view('admin.phoneTypes.edit',compact('phoneType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $phoneType=PhoneTypes::find($id);
        $phoneType->update($request->except('icon'));

        if ($request->hasFile('icon')) {
            $request->validate([

                'icon' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10) . '.' . $request->file('icon')->extension();
            $request->file('icon')->move(
                base_path() . '/public/uploads/phoneTypes/', $imageName
            );
            $phoneType->icon = '/uploads/phoneTypes/' . $imageName;
            $phoneType->save();

        }
        return redirect('/webadmin/phone_types')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل نوع الهاتف بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhoneTypes::destroy($id);
        return redirect('/webadmin/phone_types')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف نوع الهاتف بنجاح']));

    }
}
