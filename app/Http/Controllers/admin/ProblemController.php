<?php

namespace App\Http\Controllers\admin;

use App\Models\PhoneModel;
use App\Models\Problem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ProblemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $problems = Problem::paginate(10);
        return view('admin.problems.index', compact('problems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        $phoneModels = PhoneModel::all();
        return view('admin.problems.add', compact('phoneModels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required|min:3|string',
            'name_en' => 'required|min:3|string',
            'price' => 'required',
            'service_price' => 'required',
            'phone_model_id' => 'required',
        ]);
        Problem::create($request->all());
        return redirect('/webadmin/problems')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة العطل بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $problem = Problem::find($id);
        $phoneModels = PhoneModel::all();
        return view('admin.problems.edit', compact('problem', 'phoneModels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar' => 'required|min:3|string',
            'name_en' => 'required|min:3|string',
            'price' => 'required',
            'service_price' => 'required',
            'phone_model_id' => 'required',
        ]);
        Problem::find($id)->update($request->all());
        return redirect('/webadmin/problems')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل العطل بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Problem::destroy($id);
        return redirect('/webadmin/problems')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف العطل بنجاح']));
    }
}
