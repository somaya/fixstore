<?php

namespace App\Http\Controllers\admin;

use App\Models\Accessory;
use App\Models\PhoneColor;
use App\Models\PhoneModel;
use App\Models\PhoneTypes;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $accessoryTypes = Accessory::all();
        return view('admin.products.add', compact( 'accessoryTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:3|string',
            'price' => 'required',
            'description' => 'required',
            'accessory_id' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'

        ]);
        $product= Product::create([
            'title'=>$request->title,
            'price'=>$request->price,
            'description'=>$request->description,
            'accessory_id'=>$request->accessory_id,
            'category'=>3,
            'user_id'=>\Auth::user()->id,
        ]);


            $imageName = str_random(10) . '.' . $request->file('photo')->getClientOriginalExtension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/products/', $imageName
            );

            $product->photo = 'uploads/products/' . $imageName;
            $product->save();
        return redirect('/webadmin/products')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة المنتج بنجاح']));


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $users = User::where('name', '<>', null)->get();
        $phoneTypes = PhoneTypes::all();
        $phoneModels = PhoneModel::all();
        $phoneColors = PhoneColor::all();
        $accessories = Accessory::all();
        return view('admin.products.edit', compact('product', 'users', 'phoneColors', 'phoneModels', 'phoneTypes', 'accessories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|min:3|string',
            'user_id' => 'required',
            'price' => 'required',
            'address' => 'required',
            'active' => 'required',
        ]);
        $product = Product::find($id);

        $inputs = $request->all();

        if ($request->hasfile('photo') || $request->photo != null) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = str_random(10) . '.' . $request->file('photo')->getClientOriginalExtension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            if ($product->photo) {
                $old = base_path() . '/public/uploads/' . $product->photo;
                if (\File::exists($old)) {
                    unlink(base_path() . '/public/uploads/' . $product->photo);
                }
            }
            $inputs['photo'] = 'uploads/products/' . $imageName;
        }

        $product->update($inputs);

        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل المنتج بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف المنتج بنجاح']));

    }
}
