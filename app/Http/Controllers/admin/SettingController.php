<?php

namespace App\Http\Controllers\admin;

use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit($id)
    {
        $settings = Setting::find($id);
        $sliders = Slider::get();
        return view('admin.settings.edit', compact('settings', 'sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email',
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
            'snap' => 'required',
            'insta' => 'required',
            'twitter' => 'required',
            'fix_app_fees' => 'required',
            'monthly_app_fees' => 'required',
//            'item_app_fees' => 'required',
            'about_app_en' => 'required',
            'about_app_ar' => 'required',
            'terms_en' => 'required',
            'terms_ar' => 'required',
            'quality_guarantee_en' => 'required',
            'quality_guarantee_ar' => 'required',
        ]);

        $settings = Setting::find($id);
        $inputs = $request->all();

        if ($request->hasFile('logo')) {
            $request->validate([
                'logo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);


            $imageName = str_random(10) . '.' . $request->file('logo')->extension();
            $request->file('logo')->move(
                base_path() . '/public/uploads/', $imageName
            );
            if ($settings->logo) {
                if (\File::exists($settings->logo)) {
                    unlink($settings->logo);
                }
            }
            $inputs['logo'] = 'uploads/' . $imageName;
        }

        if ($request->hasFile('sliders')) {
//            $request->validate([
//                'sliders' => 'image|mimes:jpeg,png,jpg,gif,svg'
//            ]);
            $sliders = Slider::all();
            foreach ($sliders as $slider) {
                $slider->delete();
                if (\File::exists($slider->image)) {
                    unlink($slider->image);
                }
            }
            foreach ($request->file('sliders') as $newSlider) {
                $imageName = str_random(10) . '.' . $newSlider->getClientOriginalExtension();
                $newSlider->move(
                    base_path() . '/public/images/sliders/', $imageName
                );

                $slider = new Slider;
                $slider->image = 'images/sliders/' . $imageName;
                $slider->save();
            }

        }
        unset($inputs['sliders']);
        $settings->update($inputs);

        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الاعدادات بنجاح']));
    }

}
