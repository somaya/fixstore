<?php

namespace App\Http\Controllers\admin;

use App\Models\Area;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::orderBy('city_id', 'asc')->latest()->paginate(10);
        return view('admin.states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('admin.states.add', compact('cities'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'city_id' => 'required',
        ]);
        State::create($request->all());
        return redirect('/webadmin/states')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة الحى بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = City::all();
        $state = State::find($id);
        return view('admin.states.edit', compact('cities', 'state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'city_id' => 'required',
        ]);
        State::find($id)->update($request->all());
        return redirect('/webadmin/states')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الحى بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        State::destroy($id);
        return redirect('/webadmin/states')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف الحى بنجاح']));
    }
}
