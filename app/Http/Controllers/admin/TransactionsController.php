<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;

class TransactionsController extends Controller
{
    public function index()
    {
        $transactions = Transaction::orderBy('created_at', 'Desc')->paginate(10);
        $daily_transactions = Transaction::whereHas('order', function ($q) {
            $q->whereDate('created_at', Carbon::today());
        })->sum('amount');

        $monthly_transactions = Transaction::whereHas('order', function ($q) {
            $q->whereMonth('created_at', Carbon::now()->month);
        })->sum('amount');

        $yearly_transactions = Transaction::whereHas('order', function ($q) {
            $q->whereYear('created_at', date('Y'));
        })->sum('amount');


        $last_year_transactions = Transaction::whereHas('order', function ($q) {
            $q->whereYear('created_at', date('Y') - 1);
        })->sum('amount');

        return view('admin.transactions.index', compact('transactions', 'daily_transactions', 'monthly_transactions', 'yearly_transactions','last_year_transactions'));
    }

    public function destroy($id)
    {
        Transaction::destroy($id);
        return redirect('/webadmin/transactions')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف العملية بنجاح']));

    }

    public function paid($id)
    {
        Transaction::find($id)->update(['paid' => 1]);
        return redirect('/webadmin/transactions')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم جعل العملية مدفوعه بنجاح']));

    }

    public function ban($id)
    {
        $transaction = Transaction::find($id);
        if ($transaction->user->ban == 0) {
            User::find($transaction->user->id)->update(['ban' => 1]);
            return redirect('/webadmin/transactions')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم ايقاف الحساب بنجاح']));
        } else {
            User::find($transaction->user->id)->update(['ban' => 0]);
            return redirect('/webadmin/transactions')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تفعيل الحساب بنجاح']));

        }

    }
}
