<?php

namespace App\Http\Controllers\admin;

use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\City;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\Notifications\SendAdNotification;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Notification;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('role', 2)->where('type', 1)->orderBy('city_id', 'asc')->latest()->get();
        return view('admin.users.index', compact('users'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function listEngineers()
    {
        $users = User::where('role', 2)->where('type', 2)->orderBy('city_id', 'asc')->latest()->get();
        return view('admin.users.index', compact('users'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('admin.users.add', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
//            'email' => 'required|email|unique:users',
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
//            'password' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'type' => 'required',
        ]);
        $user = User::firstOrCreate([
            'name' => $request->name,
            'email' => $request->email?:null,
            'phone' => $request->phone,
            'password' => Hash::make($request->password)?:null,
            'city_id' => $request->city_id,
            'state_id' => $request->state_id,
            'type' => $request->type,
            'tokens' => Helpers::generateRandomString(),
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = str_random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }
        if ($user->type == 1)
            $redirect = 'users';
        else
            $redirect = 'engineers';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تمت الاضافة بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();
        $cities = City::all();
        return view('admin.users.edit', compact('user', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'type' => 'required',
            'phone' => [
                'required',
                'regex:/^(05|5)([0-9]{8})$/',
            ],
        ]);
        $user = User::withTrashed()->where('id', $id)->first();
        $user->update([
            'phone' => $request->phone,
            'name' => $request->name,
            'city_id' => $request->city_id,
            'state_id' => $request->state_id,
            'type' => $request->type,
        ]);
//        if ($request->email != $user->email) {
//            $this->validate($request, [
//                'email' => 'email|unique:users',
//            ]);
//            $user->update([
//                'email' => $request->email
//            ]);
//
//        }
//        if ($request->password != '') {
//            $user->update([
//                'password' => Hash::make($request->password),
//            ]);
//        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }
        if ($user->type == 1)
            $redirect = 'users';
        else
            $redirect = 'engineers';

        return redirect('/webadmin/' . $redirect)->withFlashMessage(json_encode(['success' => true, 'msg' => 'تمت الاضافة بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم الحذف بنجاح']));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function showNotification()
    {
        return view('admin.notifications.notification');
    }

    /**
     * @param Request $request
     */
    public function sendNotification(Request $request)
    {
        if ($request->user_type != 3)
            $users = User::where('ban', 0)->where('type', $request->user_type)->get();
        else
            $users = User::where('ban', 0)->get();

        foreach ($users as $user) {
            //notification
            $title = 'فيكس ستور';
            $content = $request->noti_content;
            $message = [
                "data" => strip_tags($request->message),
                "type" => 'normal_notification',
            ];
            Notification::send($user, new SendAdNotification(strip_tags($request->message)));
            \Helpers::fcm_notification($user->device_token, $content, $title, $message);
        }

        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم الارسال بنجاح']));
    }

}
