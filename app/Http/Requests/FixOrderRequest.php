<?php

namespace App\Http\Requests;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class FixOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "phone_type_id" => "required",
            "phone_model_id" => "required",
            "phone_color_id" => "required",
            "problem_id" => "required",
            "address_id" => "required",
//            "problem_details" => "required",
            "service_type" => "required",
        ];
    }
    protected function failedValidation (Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()], 400));
    }
    public function messages ()
    {
        return [
            "phone_type_id.required" => trans('messages.fix_order_phone_type_id_required'),
            "phone_model_id.required" => trans('messages.fix_order_phone_model_id_required'),
            "phone_color_id.required" => trans('messages.fix_order_phone_color_id_required'),
            "problem_id.required" => trans('messages.fix_order_problem_id_required'),
            "address_id.required" => trans('messages.fix_order_address_id_required'),
            "service_type.required" => trans('messages.fix_order_service_type_required'),
//            "problem_details.required" => trans('messages.fix_order_problem_details_required'),
        ];
    }
}
