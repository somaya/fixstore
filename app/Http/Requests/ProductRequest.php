<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'product_id' => 'required',
            'category' => 'required',
            'title' => 'required',
            'photo' => 'required',
            'price' => 'required',
            'description' => 'required',
        ];

        if (\request()->category != 3) {
            $rules['phone_type_id'] = 'required';
            $rules['phone_model_id'] = 'required';
            $rules['phone_color_id'] = 'required';
            $rules['rams'] = 'required';
            $rules['memory'] = 'required';
            $rules['processor'] = 'required';
            $rules['screen'] = 'required';
        } else {
            $rules['accessory_id'] = 'required';
        }
        return $rules;
    }


    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()
        ], JsonResponse::HTTP_BAD_REQUEST));
    }
}
