<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Accessories extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->header('Accept-Language') == 'ar') {
            $name = $this->name_ar;
        } else {
            $name = $this->name_en;
        }
        return [
            "id" => (int)$this->id,
            "name" => (string)$name,
        ];

    }
}
