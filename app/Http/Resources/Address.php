<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Address extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'city' => App()->getLocale()=='ar'?$this->city->name_ar:$this->city->name_en,
            'full_address' =>$this->full_address,
            'latitude' =>(string)$this->latitude,
            'longitude' =>(string)$this->longitude,
        ];
    }
}
