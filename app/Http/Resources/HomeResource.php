<?php

namespace App\Http\Resources;

use App\Models\Slider;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $appSliders = Slider::get();
        foreach ($appSliders as $slider) {
            $sliders[] = \Helpers::base_url() . '/' . $slider->image;
        }
        return [
            'logo' => \Helpers::base_url() . '/' . $this->logo,
            'slider' => $sliders,
            'snap' => $this->snap,
            'insta' => $this->insta,
            'twitter' => $this->twitter,
        ];
    }
}
