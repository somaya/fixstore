<?php

namespace App\Http\Resources;

use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $service_type = $this->service_type;
        if (request()->header('Accept-Language') == 'ar') {
            $type = $this->type->name_ar;
            $model = $this->model->name_ar;
            $color = $this->color->name_ar;
            $problem = $this->problem->name_ar;
            $service_type == 1 ? $service_type = 'صيانة في الموقع' : $service_type = 'صيانة في مركز الصيانة';
            $city = $this->address->city->name_ar;
            $state = $this->address->state->name_ar;

        } else {
            $type = $this->type->name_en;
            $model = $this->model->name_en;
            $color = $this->color->name_en;
            $problem = $this->problem->name_en;
            $service_type == 1 ? $service_type = 'On-site maintenance' : $service_type = 'Maintenance at the maintenance center';
            $city = $this->address->city->name_en;
            $state = $this->address->state->name_en;
        }

        $data = [
            "id" => $this->id,
            "order_number" => $this->order_number,
            "date" => $this->created_at->format('Y/m/d'),
            "price" => (double)number_format($this->price, 2, '.', ''),
            "service_price" => (double)number_format($this->service_price, 2, '.', ''),
            "status" => $this->status,
            "phone_type" => $type,
            "phone_model" => $model,
            "phone_color" => $color,
            "problem" => $problem,
            "problem_details" => $this->problem_details ?:"",
            "service_type" => $service_type,
        ];

        if ($this->status == 'Completed') {
            $rating = Rating::where('order_id', $this->id)->
            where('user_id', $this->user->id)->first();
            $data['rating'] = $rating ? (double)$rating->degree : 0;
            $data['rating_comment'] = $rating ? $rating->comment : '';
        }
        if ($this->status != 'Pending') {
            $data['engineer'] = $this->engineer ? $this->engineer->name : '';
            $data['engineer_phone'] = $this->engineer ? $this->engineer->phone : '';
        }

        $data['user'] = $this->user ? $this->user->name : '';
        $data['user_address'] = $this->address ? $this->address->full_address . ' - ' . $state . ' - ' . $city : '';
        $data['user_image'] = $this->user->photo ? \Helpers::base_url() . '/' . $this->user->photo : '';

        if ($this->status == 'Active') {
            $tracking = [];
            if ($this->tracking == 1) {
                $tracking['accept'] = false;
                $tracking['received_from_client'] = false;
                $tracking['done'] = false;
                $tracking['delivered_to_client'] = false;
            } elseif ($this->tracking == 2) {
                $tracking['accept'] = true;
                $tracking['received_from_client'] = false;
                $tracking['done'] = false;
                $tracking['delivered_to_client'] = false;
            } elseif ($this->tracking == 3) {
                $tracking['accept'] = true;
                $tracking['received_from_client'] = true;
                $tracking['done'] = false;
                $tracking['delivered_to_client'] = false;
            } elseif ($this->tracking == 4) {
                $tracking['accept'] = true;
                $tracking['received_from_client'] = true;
                $tracking['done'] = true;
                $tracking['delivered_to_client'] = false;
            } else {
                $tracking['accept'] = true;
                $tracking['received_from_client'] = true;
                $tracking['done'] = true;
                $tracking['delivered_to_client'] = true;
            }
            $data['tracking'] = $tracking;
        }

        return $data;
    }
}
