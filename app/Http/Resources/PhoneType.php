<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PhoneType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => App()->getLocale()=='ar'?$this->name_ar:$this->name_en,
            'icon' => \Helpers::base_url() . '/' . $this->icon,
            'phone_models' => new PhoneModelCollection($this->phoneModels),
        ];
    }
}
