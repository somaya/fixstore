<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Problem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => App()->getLocale()=='ar'?$this->name_ar:$this->name_en,
            'price' => (double)$this->price,
            'service_price' => (double)$this->service_price,
        ];
    }
}
