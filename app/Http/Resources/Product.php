<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'is_fav' => $this->liked(),
            'phone_type' => $this->phoneType ? \Helpers::base_url() . '/' . $this->phoneType->icon : '',
            'phone_type_name' => $this->phoneType ? (App()->getLocale() == 'ar' ? $this->phoneType->name_ar : $this->phoneType->name_en) : '',
            'phone_type_id' => $this->phoneType ? $this->phoneType->id : 0,
            'accessory_type' => $this->accessoryType ? (App()->getLocale() == 'ar' ? $this->accessoryType->name_ar : $this->accessoryType->name_en) : '',
            'phone_model' => $this->phoneModel ? (App()->getLocale() == 'ar' ? $this->phoneModel->name_ar : $this->phoneModel->name_en) : '',
            'phone_model_id' => $this->phoneModel ? $this->phoneModel->id : 0,
            'phone_color' => $this->phoneColor ? (App()->getLocale() == 'ar' ? $this->phoneColor->name_ar : $this->phoneColor->name_en) : '',
            'phone_color_id' => $this->phoneColor ? $this->phoneColor->id : 0,
            'photo' => \Helpers::base_url() . '/' . $this->photo,
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
            'address' => $this->address,
            'rams' => $this->rams ? $this->rams : '',
            'processor' => $this->processor ? $this->processor : '',
            'memory' => $this->memory ? $this->memory : '',
            'screen' => $this->screen ? $this->screen : '',
            'user' => $this->user->name,
            'user_phone' => $this->user->phone,
            'user_image' => $this->user->photo ? \Helpers::base_url() . '/' . $this->user->photo : '',
            'joined_from' => $this->user->created_at ? $this->user->created_at->toDateString() : '',
        ];
    }
}
