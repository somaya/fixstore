<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logo' => \Helpers::base_url() . '/' . $this->logo,
            'email' => $this->email,
            'phone' => $this->phone,
            'snap' => $this->snap,
            'insta' => $this->insta,
            'twitter' => $this->twitter,
            'item_app_fees' => (double)$this->item_app_fees,
            'monthly_app_fees' => (double)$this->monthly_app_fees,
            'fix_app_fees' => (double)$this->fix_app_fees,
            'terms' => \Helpers::getCurrentLang() == 'ar' ? $this->terms_ar : $this->terms_en,
            'about_app' => \Helpers::getCurrentLang() == 'ar' ? $this->about_app_ar : $this->about_app_en,
            'quality_guarantee' => \Helpers::getCurrentLang() == 'ar' ? $this->quality_guarantee_ar : $this->quality_guarantee_en,

//            'terms' => \Helpers::getCurrentLang() == 'ar' ? str_replace("&nbsp;", " ", strip_tags($this->terms_ar)) : str_replace("&nbsp;", " ", strip_tags($this->terms_en)),
//            'about_app' => \Helpers::getCurrentLang() == 'ar' ? str_replace("&nbsp;", " ", strip_tags($this->about_app_ar)) : str_replace("&nbsp;", " ", strip_tags($this->about_app_en)),
//            'quality_guarantee' => \Helpers::getCurrentLang() == 'ar' ? str_replace("&nbsp;", " ", strip_tags($this->quality_guarantee_ar)) : str_replace("&nbsp;", " ", strip_tags($this->quality_guarantee_en)),
        ];
    }
}
