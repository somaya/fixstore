<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => -$this->amount,
            'paid' => (double)$this->paid,
            'order_number' => $this->order->order_number,
            'total_price' => $this->order->price + $this->order->service_price,
            'order_date' => $this->order->created_at ? $this->order->created_at->toDateString() : '',
        ];
    }
}
