<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class  UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->header('Accept-Language') == 'ar') {
            $city = $this->city->name_ar;
            $state = $this->state->name_ar;
        } else {
            $city = $this->city->name_en;
            $state = $this->state->name_en;
        }
        return [
            "id" => (int)$this->id,
            "name" => (string)$this->name,
            "email" => (string)$this->email,
            "image" => $this->photo ? \Helpers::base_url() . '/' . $this->photo : '',
            "phone" => (string)$this->phone,
            "city" => (string)$city,
            "city_id" => (int)$this->city_id,
            "state" => (string)$state,
            "state_id" => (int)$this->state_id,
            "balance" => (double)$this->balance() ,
        ];

    }
}
