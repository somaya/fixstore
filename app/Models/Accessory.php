<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accessory extends Model
{
    protected $table = 'accessoriesTypes';
    protected $guarded = [];
    public $timestamps = true;

}
