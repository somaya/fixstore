<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adv extends Model
{
    protected $table = 'advs';
    protected $guarded = [];

    public $timestamps = true;

}
