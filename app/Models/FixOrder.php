<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FixOrder extends Model
{
    protected $table = 'orders';
    protected $guarded = [];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function engineer()
    {
        return $this->belongsTo(User::class, 'engineer_id');
    }

    public function type()
    {
        return $this->belongsTo(PhoneTypes::class, 'phone_type_id');
    }

    public function model()
    {
        return $this->belongsTo(PhoneModel::class, 'phone_model_id');
    }

    public function color()
    {
        return $this->belongsTo(PhoneColor::class, 'phone_color_id');
    }

    public function problem()
    {
        return $this->belongsTo(Problem::class, 'problem_id');
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class, 'address_id');
    }

}
