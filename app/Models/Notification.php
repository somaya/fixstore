<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $keyType = 'string';

    public $incrementing = false;


    public function getSender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id', 'id');
    }

    public function getReciever()
    {
        return $this->belongsTo(User::class, 'notifiable_id', 'id');
    }
}
