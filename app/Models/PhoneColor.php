<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneColor extends Model
{
    protected $table = 'phone_colors';
    protected $guarded = [];

    public $timestamps = true;

    public function phoneModel()
    {
        return $this->belongsTo(PhoneModel::class, 'phone_model_id');
    }

}
