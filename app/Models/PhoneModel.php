<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneModel extends Model
{
    protected $table = 'phone_models';
    protected $guarded = [];

    public $timestamps = true;

    public function phoneColors()
    {
        return $this->hasMany(PhoneColor::class, 'phone_model_id');
    }
    public function phoneType()
    {
        return $this->belongsTo(PhoneTypes::class, 'phone_type_id');
    }
}
