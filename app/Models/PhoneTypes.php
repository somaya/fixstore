<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneTypes extends Model
{
    protected $table = 'phone_types';
    protected $guarded = [];

    public $timestamps = true;

    public function phoneModels()
    {
        return $this->hasMany(PhoneModel::class, 'phone_type_id');
    }
}
