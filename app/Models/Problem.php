<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
    protected $table = 'problems';
    protected $guarded = [];
    public $timestamps = true;

    public function phoneModel()
    {
        return $this->belongsTo(PhoneModel::class, 'phone_model_id');
    }

}
