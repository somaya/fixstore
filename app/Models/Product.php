<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function phoneType()
    {
        return $this->belongsTo(PhoneTypes::class, 'phone_type_id');
    }

    public function phoneModel()
    {
        return $this->belongsTo(PhoneModel::class, 'phone_model_id');
    }
    public function phoneColor()
    {
        return $this->belongsTo(PhoneColor::class, 'phone_color_id');
    }
    public function accessoryType()
    {
        return $this->belongsTo(Accessory::class, 'accessory_id');
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class, 'address_id');
    }

    public function liked()
    {
        $user = \Helpers::getLoggedUser();
        if ($user || $user != 'No results') {
          return  Favourite::where('user_id',$user->id)->where('product_id',$this->id)->exists();
        }
        else{
            return false;

        }
    }
}
