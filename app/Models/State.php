<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $guarded = [];

    public $timestamps = true;

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
