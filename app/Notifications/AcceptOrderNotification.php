<?php

namespace App\Notifications;

use App\Models\FixOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class AcceptOrderNotification extends Notification
{
    use Queueable;

    protected $order;

    public function __construct(FixOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'id' => $this->order->id,
            'name' => 'تم قبول طلبك',
            'data' => $this->order->engineer->name . ' بواسطة المهندس ' . $this->order->order_number . 'تم قبول الطلب رقم ',
            'type' => 'accept_order'
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
