<?php

namespace App\Notifications;

use App\Models\FixOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class SendAdNotification extends Notification
{
    use Queueable;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'data' => $this->data,
            "type" => 'normal_notification',
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
