<?php

namespace App\Notifications;

use App\Models\FixOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class TrackOrderNotification extends Notification
{
    use Queueable;

    protected $order;

    public function __construct(FixOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'id' => $this->order->id,
            'name' => \Helpers::getOrderStatus($this->order->status) . ' طلبكم الان ',
            'data' => \Helpers::getOrderStatus($this->order->status) . ' حالته ' . $this->order->order_number . 'الطلب رقم ',
            'type' => 'track_order'
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
