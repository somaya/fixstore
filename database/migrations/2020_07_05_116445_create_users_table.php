<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone');
            $table->string('photo')->nullable();
            $table->tinyInteger('role')->default(2);//1 for admin,  2 for user
            $table->tinyInteger('new')->default(1);
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->tinyInteger('type')->default(1);//1 for client ,2 for engineer
            $table->string('tokens');
            $table->string('login_code')->nullable();
            $table->timestamp('code_expire_at')->nullable();
            $table->timestamp('member_at')->nullable();

            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('state_id')->nullable();
            $table->foreign('state_id')
                ->references('id')->on('states')
                ->onDelete("cascade")
                ->onUpdate("cascade");
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
