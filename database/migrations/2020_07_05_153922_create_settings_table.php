<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo');
            $table->string('slider');
            $table->string('email');
            $table->string('phone');
            $table->string('snap');
            $table->string('insta');
            $table->string('twitter');
            $table->text('terms_ar');
            $table->text('terms_en');
            $table->text('about_app_en');
            $table->text('about_app_ar');
            $table->text('quality_guarantee_ar');
            $table->text('quality_guarantee_en');
            $table->float('item_app_fees');
            $table->float('fix_app_fees');
            $table->float('monthly_app_fees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
