<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_type_id')->nullable();
            $table->foreign('phone_type_id')
                ->references('id')->on('phone_types')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_model_id')->nullable();
            $table->foreign('phone_model_id')
                ->references('id')->on('phone_models')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_color_id')->nullable();
            $table->foreign('phone_color_id')
                ->references('id')->on('phone_colors')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('problem_id')->nullable();
            $table->foreign('problem_id')
                ->references('id')->on('problems')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('address_id')->nullable();
            $table->foreign('address_id')
                ->references('id')->on('user_addresses')
                ->onDelete("cascade")
                ->onUpdate("cascade");


            $table->float('price');
            $table->float('service_price');
            $table->tinyInteger('service_type');
            $table->text('problem_details');
            $table->string('order_number');
            $table->enum('status',['Pending','Active','Completed','Canceled']);



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
