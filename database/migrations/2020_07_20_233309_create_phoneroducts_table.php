<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneroductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo');
            $table->string('title');
            $table->float('price');
            $table->text('description');
            $table->string('rams')->nullable();
            $table->string('processor')->nullable();
            $table->string('memory')->nullable();
            $table->string('screen')->nullable();
            $table->tinyinteger('category');//1 for new,2 for used,3 for accessories
            $table->tinyinteger('active')->default(1);

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_type_id')->nullable();
            $table->foreign('phone_type_id')
                ->references('id')->on('phone_types')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_model_id')->nullable();
            $table->foreign('phone_model_id')
                ->references('id')->on('phone_models')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('phone_color_id')->nullable();
            $table->foreign('phone_color_id')
                ->references('id')->on('phone_colors')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('accessory_id')->nullable();
            $table->foreign('accessory_id')
                ->references('id')->on('accessoriesTypes')
                ->onDelete("cascade")
                ->onUpdate("cascade");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
