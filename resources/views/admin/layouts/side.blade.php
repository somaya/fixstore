<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرئيسية</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/settings/1/edit')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-customer"></i>
        <span class="m-menu__link-text">الاعضاء</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/users')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">الاعضاء</span>
                </a>
            </li>
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/engineers')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">المهندسين</span>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/cities')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-home-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المناطق</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/states')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-home-2"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاحياء</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/phone_types')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">انواع الهواتف</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-technology-2"></i>
        <span class="m-menu__link-text">موديلات الهواتف</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_models')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">ابل</span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_models/samsung')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">سامسونج</span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_models/huawei')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">هواوي</span>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-technology-2"></i>
        <span class="m-menu__link-text">الوان الهواتف</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_colors')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">ابل</span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_colors/samsung')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">سامسونج</span>
                </a>
            </li>

            <li class="m-menu__item" aria-haspopup="true">
                <a href="{{url('/webadmin/phone_colors/huawei')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                    <span class="m-menu__link-text">هواوي</span>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/accessory_types')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-cart"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاكسسوارات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/contacts" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-email"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">اتصل بنا</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/problems')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-warning"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعطال</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/orders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-cart"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الطلبات</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/products')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-shopping-basket"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المنتجات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/transactions')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-coins"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">العمولات</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/notifications')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-bell"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">ارسال الاشعارات</span>
            </span>
        </span>
    </a>
</li>
