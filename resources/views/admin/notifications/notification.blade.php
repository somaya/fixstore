@extends('admin.layouts.app')

@section('title')
    ارسال الاشعارات
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">ارسال اشعارات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        ارسال اشعار
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" action="/webadmin/send_notification" method="post">
            {{ csrf_field() }}
            <div class="m-portlet__body">

                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label">ارسال اشعار الي :</label>
                    <div class="col-lg-10{{ $errors->has('user_type') ? ' has-danger' : '' }}">
                        <select name="user_type" class="form-control">
                            <option value="" disabled selected>اختر نوع الاعضاء</option>
                            <option value="1">الاعضاء</option>
                            <option value="2">المهندسين</option>
                            <option value="3">الكل</option>
                        </select>
                        @if ($errors->has('user_type'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('user_type') }}</strong>
            </span>
                        @endif
                    </div>
                </div>


                <div class="form-group m-form__group row">

                    <label class="col-lg-2 col-form-label">عنوان الاشعار </label>
                    <div class="col-lg-10{{ $errors->has('noti_content') ? ' has-danger' : '' }}">
                        <input type="text" name="noti_content" id="noti_content" class="form-control m-input"
                               placeholder="ادخل عنوان الاشعار">
                        @if ($errors->has('noti_content'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('noti_content') }}</strong>
            </span>
                        @endif
                    </div>

                </div>

                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label"> نص الاشعار: </label>
                    <div class="col-lg-10{{ $errors->has('message') ? ' has-danger' : '' }}">
                        {!! Form::textarea('message',old('message'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'ادخل نص الاشعار' ]) !!}
                        @if ($errors->has('message'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
                        @endif
                    </div>
                </div>


            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">ارسال</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->

@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

