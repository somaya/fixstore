@extends('admin.layouts.app')
@section('title')
    الطلبات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/orders')}}" class="m-menu__link">
            <span class="m-menu__link-text">الطلبات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الطلبات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {{--            <div><a href="{{route('orders.create')}}" style="margin-bottom:20px"--}}
            {{--                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i>Add Order</a></div>--}}
            {{--            <div class="contant">--}}
            {{--                <p>Filter By Status</p>--}}
            {{--                <div class="row">--}}

            {{--                    <div class="col-lg-4">--}}

            {{--                        <select id="search_key" class="form-control">--}}
            {{--                            <option disabled selected>Select Status</option>--}}
            {{--                            <option value="1">Verified</option>--}}
            {{--                            <option value="2">Waiting For Verify</option>--}}
            {{--                            <option value="3">Unverified</option>--}}
            {{--                        </select>--}}
            {{--                    </div>--}}

            {{--                </div>--}}
            {{--            </div>--}}
            <br>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>تاريخ الطلب</th>
                    <th>صاحب الطلب</th>
                    <th>المهندس</th>
                    <th>سعر العطل</th>
                    <th>سعر الخدمة</th>
                    <th>السعر الكلي</th>
                    <th>حالة الطلب</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->order_number}}</td>
                        <td>{{$order->created_at->format('Y/m/d')}} </td>
                        <td>{{$order->user ? $order->user->name : '--------'}} </td>
                        <td>{{$order->engineer ? $order->engineer->name : '--------'}} </td>
                        <td>{{$order->price}} </td>
                        <td>{{$order->service_price}} </td>
                        <td>{{$order->price + $order->service_price}} </td>
                        <td>{{Helpers::getOrderStatus($order->status)}} </td>
                        <td>


                            {{--                            <a title="Show Details" href="/webadmin/orders/{{$order->id}}"><i class="fa fa-eye"></i></a>--}}
                            {{--<a  title="Bank Data" href="/webadmin/order/{{$order->id}}/bank-data" ><i class="fa fa-credit-card"></i></a>--}}
                            <a title="Edit" href="/webadmin/orders/{{$order->id}}/edit"><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/orders/{{ $order->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
