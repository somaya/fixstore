`@extends('admin.layouts.app')

@section('title')
    تفاصيل الطلب
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/order')}}" class="m-menu__link">
            <span class="m-menu__link-text">الطلبات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل الطلب</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل الطلب
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($order,['route' => ['orders.show' , $order->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label"> رقم الطلب </label>
                <div class="col-lg-5{{ $errors->has('order_number') ? ' has-danger' : '' }}">
                    {!! Form::text('order_number',old('order_number'),['class'=>'form-control m-input','autofocus','disabled','placeholder'=> 'Name' ]) !!}
                    @if ($errors->has('order_number'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('order_number') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">صاحب الطلب</label>
                <div class="col-lg-5{{ $errors->has('user_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->user->name}}" disabled>

                    @if ($errors->has('user_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
                    @endif
                </div>

            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label"> نوع الهاتف </label>
                <div class="col-lg-3{{ $errors->has('phone_type_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->type->name_ar}}" disabled>
                    @if ($errors->has('phone_type_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_type_id') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">موديل الهاتف </label>
                <div class="col-lg-3{{ $errors->has('phone_model_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->model->name_ar}}" disabled>
                    @if ($errors->has('phone_model_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_model_id') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">لون الهاتف </label>
                <div class="col-lg-3{{ $errors->has('phone_color_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->color->name_ar}}" disabled>
                    @if ($errors->has('phone_color_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_color_id') }}</strong>
            </span>
                    @endif
                </div>

            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label"> العطل </label>
                <div class="col-lg-5{{ $errors->has('problem_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->problem->name_ar}}" disabled>
                    @if ($errors->has('problem_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('problem_id') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">العنوان </label>
                <div class="col-lg-5{{ $errors->has('address_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->address->city->name_ar}}" disabled>
                    @if ($errors->has('address_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address_id') }}</strong>
            </span>
                    @endif
                </div>

            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label"> السعر </label>
                <div class="col-lg-5{{ $errors->has('price') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->price}}" disabled>
                    @if ($errors->has('price'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">حالة الطلب</label>
                <div class="col-lg-5{{ $errors->has('status') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->status}}" disabled>
                    @if ($errors->has('status'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
                    @endif
                </div>

            </div>


        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

`
