<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">لون الهاتف بالعربية: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"لون الهاتف بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">لون الهاتف بالانجليزية : </label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"لون الهاتف بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">موديل الهاتف : </label>
<div class="col-lg-10{{ $errors->has('phone_model_id') ? ' has-danger' : '' }}">
    <select name="phone_model_id" class="form-control">
        <option value="" disabled selected>اختر موديل الهاتف</option>
        @foreach($phoneModels as $phoneModel)
        <option value="{{$phoneModel->id}}" {{isset($phoneColor) && $phoneColor->phone_model_id==$phoneModel->id?'selected':'' }} {{old('phone_model_id')==$phoneModel->id? 'selected':''}}>{{$phoneModel->name_ar}}</option>
            @endforeach

    </select>
    @if ($errors->has('phone_model_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_model_id') }}</strong>
            </span>
    @endif
</div>
</div>
