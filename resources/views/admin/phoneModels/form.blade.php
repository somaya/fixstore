<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">موديل الهاتف بالعربية: </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"موديل الهاتف بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">موديل الهاتف بالانجليزية : </label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"مودل الهاتف بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">نوع الهاتف : </label>
<div class="col-lg-10{{ $errors->has('phone_type_id') ? ' has-danger' : '' }}">
    <select name="phone_type_id" class="form-control">
        <option value="" disabled selected>اختر نوع الهاتف</option>
        @foreach($phoneTypes as $phoneType)
        <option value="{{$phoneType->id}}" {{isset($phoneModel) && $phoneModel->phone_type_id==$phoneType->id?'selected':'' }} {{old('phone_type_id')==$phoneType->id? 'selected':''}}>{{$phoneType->name_ar}}</option>
            @endforeach

    </select>
    @if ($errors->has('phone_type_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_type_id') }}</strong>
            </span>
    @endif
</div>
</div>
