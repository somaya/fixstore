<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">نوع الهاتف بالعربية : </label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"نوع الهاتف بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">نوع الهاتف بالانجليزية : </label>
    <div class="col-lg-5{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"نوع الهاتف بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الايقونة: </label>
    <div class="col-lg-10{{ $errors->has('icon') ? ' has-danger' : '' }}">

        <input type="file" name="icon"   class="form-control uploadinput">
        @if ($errors->has('icon'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($phoneType) && $phoneType->icon)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$phoneType->icon}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




