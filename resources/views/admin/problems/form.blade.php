<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم العطل بالعربية</label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم العطل بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">اسم العطل بالانجليزية</label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"اسم العطل بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">السعر</label>
    <div class="col-lg-4{{ $errors->has('price') ? ' has-danger' : '' }}">
        {!! Form::text('price',null,['class'=>'form-control m-input','autofocus','placeholder'=>"100"]) !!}
        @if ($errors->has('price'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">سعر الخدمة</label>
    <div class="col-lg-4{{ $errors->has('service_price') ? ' has-danger' : '' }}">
        {!! Form::text('service_price',null,['class'=>'form-control m-input','placeholder'=>"50"]) !!}
        @if ($errors->has('service_price'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('service_price') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">موديل الهاتف</label>
    <div class="col-lg-10{{ $errors->has('phone_model_id') ? ' has-danger' : '' }}">
        <select name="phone_model_id" id="phone_model_id" class="form-control m-input">
            <option value="">--اختر الموديل --</option>
            @foreach($phoneModels as $phoneModel)
                <option
                    value="{{$phoneModel->id}}" {{isset($problem) && $problem->phone_model_id==$phoneModel->id ?'selected':''}}>{{$phoneModel->name_ar}}</option>
            @endforeach

        </select>

        @if ($errors->has('phone_model_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_model_id') }}</strong>
            </span>
        @endif
    </div>

</div>




