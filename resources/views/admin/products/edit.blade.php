@extends('admin.layouts.app')

@section('title')
    تعديل منتج
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/order')}}" class="m-menu__link">
            <span class="m-menu__link-text">المنتجات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تعديل منتج</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تعديل منتج
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($product,['route' => ['products.update' , $product->id],'method'=> 'PATCH','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">

            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">العنوان </label>
                <div class="col-lg-5{{ $errors->has('title') ? ' has-danger' : '' }}">
                    {!! Form::text('title',old('title'),['class'=>'form-control m-input','autofocus',' ','placeholder'=> 'عنوان' ]) !!}
                    @if ($errors->has('title'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">صاحب المنتج</label>
                <div class="col-lg-5{{ $errors->has('user_id') ? ' has-danger' : '' }}">

                    <select name="user_id" id="" class="form-control m-input--solid">
                        @foreach($users as $user)
                            <option value="{{$user->id}}"
                                    @if($user->id == $product->user->id) selected @endif>{{$user->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('user_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
                    @endif
                </div>

            </div>


            <div class="form-group m-form__group row">
                @if(!$product->accessory_id)
                    <label class="col-lg-1 col-form-label">نوع الهاتف </label>
                    <div class="col-lg-3{{ $errors->has('phone_type_id') ? ' has-danger' : '' }}">
                        <select name="phone_type_id" class="form-control m-input--solid">
                            @foreach($phoneTypes as $type)
                                <option value="{{ $type->id }}"
                                        @if($type->id == $product->phoneType->id)selected @endif>
                                    {{ $type->name_ar }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('phone_type_id'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_type_id') }}</strong>
            </span>
                        @endif
                    </div>

                    <label class="col-lg-1 col-form-label"> موديل الهاتف </label>
                    <div class="col-lg-3{{ $errors->has('phone_model_id') ? ' has-danger' : '' }}">
                        <select name="phone_model_id" class="form-control m-input--solid">
                            @foreach($phoneModels as $model)
                                <option value="{{ $model->id }}"
                                        @if($model->id == $product->phoneModel->id)selected @endif>
                                    {{ $model->name_ar }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('phone_model_id'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_model_id') }}</strong>
            </span>
                        @endif
                    </div>

                    <label class="col-lg-1 col-form-label"> لون الهاتف </label>
                    <div class="col-lg-3{{ $errors->has('phone_color_id') ? ' has-danger' : '' }}">
                        <select name="phone_color_id" class="form-control m-input--solid">
                            @foreach($phoneColors->unique('name_ar') as $color)
                                <option value="{{ $color->id }}"
                                        @if($color->id == $product->phoneColor->id)selected @endif>
                                    {{ $color->name_ar }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('phone_color_id'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone_color_id') }}</strong>
            </span>
                        @endif
                    </div>

                @else

                    <label class="col-lg-1 col-form-label"> الاكسسوار </label>
                    <div class="col-lg-11{{ $errors->has('accessory_id') ? ' has-danger' : '' }}">
                        <select name="accessory_id" class="form-control m-input--solid">
                            @foreach($accessories as $accessory)
                                <option value="{{ $accessory->id }}"
                                        @if($accessory->id == $product->accessoryType->id)selected @endif>
                                    {{ $accessory->name_ar }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('accessory_id'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('accessory_id') }}</strong>
            </span>
                        @endif
                    </div>

                @endif
            </div>


            @if(!$product->accessory_id)
                <div class="form-group m-form__group row">
                    <label class="col-lg-1 col-form-label"> الرام</label>
                    <div class="col-lg-5{{ $errors->has('rams') ? ' has-danger' : '' }}">
                        {!! Form::text('rams',old('rams'),['class'=>'form-control m-input','autofocus',' ','placeholder'=> 'Rams' ]) !!}
                        @if ($errors->has('rams'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('rams') }}</strong>
            </span>
                        @endif
                    </div>
                    <br>

                    <label class="col-lg-1 col-form-label"> المعالج </label>
                    <div class="col-lg-5{{ $errors->has('processor') ? ' has-danger' : '' }}">
                        {!! Form::text('processor',old('processor'),['class'=>'form-control m-input','autofocus',' ','placeholder'=> 'Processor' ]) !!}
                        @if ($errors->has('processor'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('processor') }}</strong>
            </span>
                        @endif
                    </div>
                    <br>


                </div>

                <div class="form-group m-form__group row">
                    <label class="col-lg-1 col-form-label"> مساحة التخزين </label>
                    <div class="col-lg-5{{ $errors->has('memory') ? ' has-danger' : '' }}">
                        {!! Form::text('memory',old('memory'),['class'=>'form-control m-input','autofocus',' ','placeholder'=> 'Memory' ]) !!}
                        @if ($errors->has('memory'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('memory') }}</strong>
            </span>
                        @endif
                    </div>


                    <label class="col-lg-1 col-form-label"> الشاشة </label>
                    <div class="col-lg-5{{ $errors->has('screen') ? ' has-danger' : '' }}">
                        {!! Form::text('screen',old('screen'),['class'=>'form-control m-input','autofocus',' ','placeholder'=> 'Screen' ]) !!}
                        @if ($errors->has('screen'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('screen') }}</strong>
            </span>
                        @endif
                    </div>
                </div>
            @endif


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">العنوان </label>
                <div class="col-lg-5{{ $errors->has('address') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" name="address" value="{{$product->address}}">
                    @if ($errors->has('address'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
                    @endif
                </div>

                <label class="col-lg-1 col-form-label"> السعر </label>
                <div class="col-lg-5{{ $errors->has('price') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" name="price" value="{{$product->price}}">
                    @if ($errors->has('price'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
                    @endif
                </div>

            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الحالة </label>
                <div class="col-lg-5{{ $errors->has('active') ? ' has-danger' : '' }}">
                    <select name="active" id="" class="form-control m-input--solid">
                        <option value="1" @if($product->active == 1) selected @endif>نشط</option>
                        <option value="0" @if($product->active == 0) selected @endif>متوقف</option>
                    </select>
                    @if ($errors->has('active'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('active') }}</strong>
            </span>
                    @endif
                </div>


                <label class="col-lg-1 col-form-label">حالة المنتج </label>
                <div class="col-lg-5{{ $errors->has('category') ? ' has-danger' : '' }}">
                    <select name="category" id="" class="form-control m-input--solid">
                        <option value="1" @if($product->category == 1) selected @endif>جديد</option>
                        <option value="2" @if($product->category == 2) selected @endif>مستعمل</option>
                        <option value="3" @if($product->category == 3) selected @endif>اكسسوارات</option>
                    </select>
                    @if ($errors->has('category'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category') }}</strong>
            </span>
                    @endif
                </div>


            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">صورة المنتج </label>
                <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

                    <input type="file" name="photo" class="form-control uploadinput">
                    @if ($errors->has('photo'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
                    @endif
                </div>

            </div>
            @if(isset($product) && $product->photo)
                <input type="hidden" value="{{ $product->photo }}" name="photo">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px; text-align: center">

                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($product->photo)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                </div>
            @endif


        </div>
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success form-control" value="edit">تعديل
                        </button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

