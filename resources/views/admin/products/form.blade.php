<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المنتج</label>
    <div class="col-lg-4{{ $errors->has('title') ? ' has-danger' : '' }}">
        {!! Form::text('title',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم المنتج "]) !!}
        @if ($errors->has('title'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">السعر</label>
    <div class="col-lg-4{{ $errors->has('price') ? ' has-danger' : '' }}">
        {!! Form::text('price',null,['class'=>'form-control m-input','placeholder'=>"السعر"]) !!}
        @if ($errors->has('price'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">التفاصيل</label>
    <div class="col-lg-10{{ $errors->has('description') ? ' has-danger' : '' }}">
{{--        {!! Form::text('service_price',null,['class'=>'form-control m-input','placeholder'=>"50"]) !!}--}}
        <textarea name="description" class="form-control m-input"></textarea>
        @if ($errors->has('description'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">نوع الاكسسوار</label>
    <div class="col-lg-10{{ $errors->has('accessory_id') ? ' has-danger' : '' }}">
        <select name="accessory_id" id="accessory_id" class="form-control m-input">
            <option value="">--اختر النوع --</option>
            @foreach($accessoryTypes as $accessoryType)
                <option
                    value="{{$accessoryType->id}}" {{isset($product) && $product->accessory_id==$accessoryType->id ?'selected':''}}>{{$accessoryType->name_ar}}</option>
            @endforeach

        </select>

        @if ($errors->has('accessory_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('accessory_id') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصوره: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($product) && $product->photo)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$product->photo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif





