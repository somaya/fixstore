@extends('admin.layouts.app')
@section('title')
    المنتجات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/products')}}" class="m-menu__link">
            <span class="m-menu__link-text">المنتجات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        المنتجات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('products.create')}}" style="margin-bottom:20px"
                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف اكسسوار</a></div>

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>العنوان</th>
                    <th>صاحب المنتج</th>
                    <th>السعر</th>
                    <th>النوع</th>
                    <th>الحالة</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->title}} </td>
                        <td>
                            <a href="users/{{$product->user_id}}">{{$product->user ? $product->user->name : '--------'}}</a>
                        </td>
                        <td>{{$product->price}} </td>
                        <td>{{$product->category  === 1 ? "New" : ($product->category == 2 ? "Used" : "Accessories") }} </td>
                        <td>{{$product->active == 1 ? 'Active' : 'Inactive'}} </td>
                        <td>

                            {{--<a  title="Bank Data" href="/webadmin/order/{{$product->id}}/bank-data" ><i class="fa fa-credit-card"></i></a>--}}
                            <a title="Edit" href="/webadmin/products/{{$product->id}}/edit"><i
                                    class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/products/{{ $product->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {{$products->links()}}
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
