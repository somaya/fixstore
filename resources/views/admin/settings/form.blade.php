<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> البريد الالكتروني </label>
    <div class="col-lg-4{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::email('email',old('email'),['class'=>'form-control m-input','autofocus','placeholder'=> 'Email' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">الهاتف </label>
    <div class="col-lg-4{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'Phone' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">سناب شات</label>
    <div class="col-lg-3{{ $errors->has('snap') ? ' has-danger' : '' }}">
        {!! Form::text('snap',old('snap'),['class'=>'form-control m-input','placeholder'=> 'Snap Chat' ]) !!}
        @if ($errors->has('snap'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('snap') }}</strong>
            </span>
        @endif
    </div>

    <label class="col-lg-1 col-form-label">انستجرام</label>
    <div class="col-lg-3{{ $errors->has('insta') ? ' has-danger' : '' }}">
        {!! Form::text('insta',old('insta'),['class'=>'form-control m-input','placeholder'=> 'Instagram' ]) !!}
        @if ($errors->has('insta'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('insta') }}</strong>
            </span>
        @endif
    </div>


    <label class="col-lg-1 col-form-label">تويتر</label>
    <div class="col-lg-3{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','placeholder'=> 'Twitter' ]) !!}
        @if ($errors->has('twitter'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> عمولة الصيانة</label>
    <div class="col-lg-4{{ $errors->has('fix_app_fees') ? ' has-danger' : '' }}">
        {!! Form::text('fix_app_fees',old('fix_app_fees'),['class'=>'form-control m-input','autofocus','placeholder'=> 'Fix Fee' ]) !!}
        @if ($errors->has('fix_app_fees'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('fix_app_fees') }}</strong>
            </span>
        @endif
    </div>

    <label class="col-lg-2 col-form-label"> العمولة الشهرية</label>
    <div class="col-lg-4{{ $errors->has('monthly_app_fees') ? ' has-danger' : '' }}">
        {!! Form::text('monthly_app_fees',old('monthly_app_fees'),['class'=>'form-control m-input','autofocus','placeholder'=> 'Monthly Fee' ]) !!}
        @if ($errors->has('monthly_app_fees'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('monthly_app_fees') }}</strong>
            </span>
        @endif
    </div>



</div>

<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">لوجو التطبيق</label>
    <div class="col-lg-11{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo" class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($settings) && $settings->logo)
    <input type="hidden" value="{{ $settings->logo }}" name="logo">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px; text-align: center">

            <img
                data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                alt="First slide [800x4a00]"
                src="{{asset($settings->logo)}}"
                style="height: 150px; width: 150px"
                data-holder-rendered="true">
        </div>

    </div>
@endif


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> عن التطبيق بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('fix_app_fees') ? ' has-danger' : '' }}">
        {!! Form::textarea('about_app_en',old('about_app_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'About App English' ]) !!}
        @if ($errors->has('about_app_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('about_app_en') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> عن التطبيق بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('fix_app_fees') ? ' has-danger' : '' }}">
        {!! Form::textarea('about_app_ar',old('about_app_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'About App Arabic' ]) !!}
        @if ($errors->has('about_app_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('about_app_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> الشروط والاحكام بالانجليزية : </label>
    <div class="col-lg-10{{ $errors->has('terms_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('terms_en',old('terms_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'Terms English' ]) !!}
        @if ($errors->has('terms_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('terms_en') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> الشروط والاحكام بالعربية : </label>
    <div class="col-lg-10{{ $errors->has('terms_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('terms_ar',old('terms_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'Terms Arabic' ]) !!}
        @if ($errors->has('terms_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('terms_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> ضمان الجودة بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('quality_guarantee_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('quality_guarantee_en',old('quality_guarantee_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'Quality Guarantee Arabic' ]) !!}
        @if ($errors->has('quality_guarantee_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('quality_guarantee_en') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
        <label class="col-lg-2 col-form-label"> ضمان الجودة بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('quality_guarantee_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('quality_guarantee_ar',old('quality_guarantee_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'Quality Guarantee Arabic' ]) !!}
        @if ($errors->has('quality_guarantee_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('quality_guarantee_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الاعلانات ( الاسلايدرات ) : </label>
    <div class="col-lg-10{{ $errors->has('sliders') ? ' has-danger' : '' }}">

        <input type="file" name="sliders[]" class="form-control uploadinput" multiple>
        @if ($errors->has('sliders'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('sliders') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($sliders))
    @foreach($sliders as $slider)
        <input type="hidden" value="{{ $slider->image }}" name="sliders[]">
    @endforeach
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: center;display: inline-flex;margin: 0 190px;">
            @foreach($sliders as $slider)
                <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{asset($slider->image)}}"
                    style="height: 150px; width: 150px; padding: 10px"
                    data-holder-rendered="true">
            @endforeach
        </div>

    </div>
@endif
