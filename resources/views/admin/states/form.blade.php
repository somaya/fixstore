<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم الحي بالعربية</label>
    <div class="col-lg-4{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم الحي بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">اسم الحي بالانلجيزية</label>
    <div class="col-lg-4{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','placeholder'=>"اسم الحي بالانلجيزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">المنطقة التابع لها</label>
<div class="col-lg-10{{ $errors->has('city_id') ? ' has-danger' : '' }}">
    <select name="city_id" class="form-control">
        <option value="" disabled selected>اختر المنطقة</option>
        @foreach($cities as $city)
        <option value="{{$city->id}}" {{isset($state) && $state->city_id==$city->id?'selected':'' }} {{old('city_id')==$city->id? 'selected':''}}>{{$city->name_ar}}</option>
            @endforeach

    </select>
    @if ($errors->has('city_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
    @endif
</div>
</div>
