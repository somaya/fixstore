@extends('admin.layouts.app')
@section('title')
    العمولات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">العمولات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        العمولات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">أرباح التطبيق اليوم</label>
                <div class="col-lg-2">

                    <span>{{$daily_transactions}}</span>
                </div>
                <label class="col-lg-1 col-form-label">أرباح التطبيق الشهرية</label>
                <div class="col-lg-2">

                    <span>{{$monthly_transactions}}</span>
                </div>
                <label class="col-lg-1 col-form-label">أرباح التطبيق السنوية</label>
                <div class="col-lg-2">
                    <span>{{$yearly_transactions}}</span>
                </div>
                <label class="col-lg-1 col-form-label">أرباح التطبيق السنوية السابقة</label>
                <div class="col-lg-2">
                    <span>{{$last_year_transactions}}</span>
                </div>
            </div>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>#</th>
                    <th>رقم الطلب</th>
                    <th>المهندس</th>
                    <th>قيمة الطلب</th>
                    <th>عمولة التطبيق</th>
                    <th>الحاله</th>
                    <th> حالة الحساب</th>
                    <th>الادوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $index=>$transaction)
                    <tr>
                        <td>{{$index + $transactions->firstItem()}}</td>
                        <td>
                            <a href="/webadmin/orders/{{$transaction->order->id}}">{{$transaction->order->order_number}}</a>
                        </td>
                        <td><a href="/webadmin/users/{{$transaction->user->id}}">{{$transaction->user->name}}</a></td>
                        <td>{{$transaction->order->price+$transaction->order->service_price}}</td>
                        <td>{{$transaction->amount}}</td>
                        <td>{{$transaction->paid==0?'غير مدفوع':'مدفوع'}}</td>
                        <td>{{$transaction->user->ban==0?'مفعل':'موقوف'}}</td>
                        <td>
                            @if($transaction->paid==0)
                                <a title="جعله مدفوع" href="/webadmin/transactions/{{$transaction->id}}/paid"><i
                                        class="fa fa-check-circle"></i></a>
                            @endif

                            <a title="{{$transaction->user->ban ==0?'ايقاف الحساب':'تفعيل الحساب'}}"
                               href="/webadmin/transactions/{{$transaction->id}}/ban"><i
                                    class="{{$transaction->user->ban==0?'fa fa-ban':'fa fa-check'}}"></i></a>


                            <form class="inline-form-style"
                                  action="/webadmin/transactions/{{ $transaction->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$transactions->links()}}
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
