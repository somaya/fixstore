@extends('admin.layouts.app')

@section('title')
    تفاصيل العضو
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/users')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاعضاء</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل العضو</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل العضو
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($user,['route' => ['users.show' , $user->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled','placeholder'=> 'فيكس  ستور' ]) !!}
                    @if ($errors->has('name'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">البريد الالكتروني </label>
                <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
                    {!! Form::text('email',old('email'),['class'=>'form-control m-input','disabled','placeholder'=> 'fixstore@fixstore.com' ]) !!}
                    @if ($errors->has('email'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif
                </div>

            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">الهاتف</label>
                <div class="col-lg-10{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled','placeholder'=> '01010101010' ]) !!}
                    @if ($errors->has('phone'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
                    @endif
                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">المنطقة </label>
                <div class="col-lg-5{{ $errors->has('city_id') ? ' has-danger' : '' }}">

                    <input type="text" class="form-control m-input" value="{{$user->city->name_ar}}" disabled>
                    @if ($errors->has('city_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
                    @endif
                </div>
                <label class="col-lg-1 col-form-label">الحي</label>
                <div class="col-lg-5{{ $errors->has('state_id') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$user->state->name_ar}}" disabled>

                    @if ($errors->has('state_id'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('state_id') }}</strong>
            </span>
                    @endif
                </div>

            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">نوع العضو </label>
                <div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$user->type==2?'مهندس':'عضو عادي'}}"
                           disabled>

                    @if ($errors->has('type'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
                    @endif
                </div>

            </div>
            @if($user->type==2)
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">الرصيد </label>
                <div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$user->balance()==0?0:-$user->balance()}}"
                           disabled>

                </div>

            </div>
            @endif

            @if(isset($user) && $user->photo)
                <div class="row">


                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->photo)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                </div>
            @endif


        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

