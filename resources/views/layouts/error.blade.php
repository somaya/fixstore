@if(Session::has('error'))
    <span class="help-block alert-danger">
        <strong>{{ Session::get('error') }}</strong>
    </span>
@endif
@if(Session::has('success'))
    <span class="help-block alert-success">
        <strong>{{ Session::get('success') }}</strong>
    </span>
@endif