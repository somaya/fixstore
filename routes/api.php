<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api', 'middleware' => 'language'], function () {

    Route::get('/update_language/{lng}', 'HomeController@updateLanguage');
// lists
    Route::get('get_cities', 'HomeController@getCities');
    Route::get('get_states/{id}', 'HomeController@getStates');
    Route::get('get_advs', 'HomeController@getAdvs');
    Route::get('get_phone_types', 'HomeController@getPhoneTypes');
    Route::get('get_phone_models/{id}', 'HomeController@getPhoneModels');
    Route::get('get_phone_colors/{id}', 'HomeController@getPhoneColors');
    Route::get('get_settings', 'HomeController@getSettings');
    Route::get('get_home', 'HomeController@getHome');
    Route::get('get_problems/{phone_model_id}', 'HomeController@getProblems');
    Route::post('contact_us', 'HomeController@contactUs');


    // user
    Route::post('register', 'UserController@register');
    Route::post('verify_code', 'UserController@verifyUser');
    Route::post('user_resend_code', 'UserController@resendUserActivationCode');
    Route::post('complete_profile', 'UserController@completeProfile');
    Route::get('logout', 'UserController@logout');

    Route::get('get_profile', 'UserController@getProfile');
    Route::post('update_profile', 'UserController@updateProfile');
    Route::post('update_password', 'UserController@updateOldPassword');
    Route::post('update_profile_image', 'UserController@changeProfilePicture');

    Route::get('get_user_addresses', 'UserController@getUserAddresses');
    Route::post('add_address', 'UserController@addAddress');
    Route::delete('delete_address/{id}', 'UserController@deleteAddress');

    // Engineers
    Route::get('get_engineer_statics', 'UserController@getEngineerStatics');


    //orders
    Route::post('add_fix_order', 'OrderController@addFixOrder');
    Route::post('rate_fix_order', 'OrderController@rateOrder');
    Route::get('get_orders/{status}', 'OrderController@getOrders');
    Route::get('get_transactions/{status}', 'OrderController@getTransactions');
    Route::get('get_order/{order_id}', 'OrderController@getOrderDetails');
    Route::get('get_transaction/{id}', 'OrderController@getTransactionDetails');
    Route::get('cancel_order/{order_id}', 'OrderController@cancelOrder');
    Route::get('accept_order/{order_id}', 'OrderController@acceptOrder');
    Route::post('change_order_track', 'OrderController@changeOrderTrack');

    //products
    Route::get('get_products/{category}', 'ProductsController@getProducts');
    Route::get('get_products/{category}/{type}', 'ProductsController@getProducts');
    Route::get('get_favourite_products', 'ProductsController@getFavouriteProducts');
    Route::post('change_favourite_product', 'ProductsController@changeFavouriteProduct');
    Route::post('add_product', 'ProductsController@addProduct');
    Route::post('edit_product', 'ProductsController@editProduct');
    Route::post('remove_favourite_product', 'ProductsController@removeFavouriteProduct');

    Route::get('get_product_details/{product_id}', 'ProductsController@getProductDetails');
    Route::get('stop_product/{product_id}', 'ProductsController@stopProduct');
    Route::get('get_my_products', 'ProductsController@getMyProducts');


    Route::get('get_accessories', 'ProductsController@getAccessories');

    // notifications
    Route::get('get_notifications', 'UserController@getNotifications');
    Route::get('/get_unread_notifications', 'UserController@unreadNotifications');
    Route::get('/delete_notifications', 'UserController@deleteNotification');
    Route::get('/delete_one_notification/{id}', 'UserController@deleteOneNotification');

});


