<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    return view('landing');
});


Auth::routes();

Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/login', function () {
        return view('welcome');
    });
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/users', 'admin\UsersController');
    Route::get('/engineers', 'admin\UsersController@listEngineers');
    Route::resource('/orders', 'admin\OrderController');
    Route::resource('/problems', 'admin\ProblemController');
    Route::resource('/cities', 'admin\CitiesController');
    Route::resource('/states', 'admin\StatesController');
    Route::resource('/phone_types', 'admin\PhoneTypesController');
    Route::resource('/settings', 'admin\SettingController');
    Route::resource('/products', 'admin\ProductController');
    Route::get('/phone_models/samsung', 'admin\PhoneModelsController@listSamsung');
    Route::get('/phone_models/huawei', 'admin\PhoneModelsController@listHuawei');
    Route::resource('/phone_models', 'admin\PhoneModelsController');
    Route::get('/phone_colors/samsung', 'admin\PhoneColorsController@listSamsung');
    Route::get('/phone_colors/huawei', 'admin\PhoneColorsController@listHuawei');
    Route::resource('/phone_colors', 'admin\PhoneColorsController');
    Route::resource('/accessory_types', 'admin\AccessoriesTypesController');
    Route::resource('/contacts', 'admin\ContactsController');
    Route::resource('/transactions', 'admin\TransactionsController');
    Route::get('/transactions/{id}/paid', 'admin\TransactionsController@paid');
    Route::get('/transactions/{id}/ban', 'admin\TransactionsController@ban');
    Route::get('/contact/{id}/reply', 'admin\ContactsController@reply');
    Route::post('/contact/{id}/sendreply', 'admin\ContactsController@sendReply');

    Route::get('/notifications', 'admin\UsersController@showNotification');
    Route::post('/send_notification', 'admin\UsersController@sendNotification');


});
Route::get('/getStates', function () {
    $city_id = Input::get('city_id');
    $states = \App\Models\State::where('city_id', '=', $city_id)->get();
    return json_encode($states);
});
